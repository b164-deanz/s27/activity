const http = require("http")
const port = 6000

let mock = [
			{
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
   			 },
			 {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
 		     }

]

let server = http.createServer((req, res) =>{
	if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(mock))
		res.end()
	}
	if(req.url == '/profile' && req.method == "POST"){
		let inputUser = '';
		req.on('data', function(data){
			inputUser += data;
		})


		req.on('end', function(){
			inputUsers = JSON.parse(inputUser)

			let userNew = {
				"firstName": inputUsers.firstName,
				"lastName": inputUsers.lastName,
				"mobileNo": inputUsers.mobileNo,
				"email": inputUsers.email,
				"password": inputUsers.password
			}

			mock.push(userNew)
			console.log(mock)


			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(userNew))
			res.end()
		})
	}

})
server.listen(port)

console.log(`Server runnong at localhost: ${port}`)